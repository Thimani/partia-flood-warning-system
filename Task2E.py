from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.station import MonitoringStation
import datetime
from datetime import timedelta
import numpy as np

def run ():
    stations = build_station_list ()
    update_water_levels (stations)
    for station in stations_highest_rel_level(stations,5):
        dates, levels = fetch_measure_levels(station[0], timedelta(days=10))
        plot_water_levels(station[2], dates, levels)



if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()