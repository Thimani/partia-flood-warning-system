from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.station import MonitoringStation
def test_stations_level_over_threshold():
# test station a. Has a no typical range values
    s_id = "s_id_A"
    m_id = "m_id_A"
    label = "station a"
    coord = (-10.54,132.9)
    trange = None
    river = "River a"
    town = "My Town a"
    a = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    #test station B. Has no relative water level
    s_id = "s_id_B"
    m_id = "m_id_B"
    label = "station b"
    coord = (28.537,-10.87)
    trange = (0, 2)
    river = "River b"
    town = "My Town b"
    b = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # test station C. Has a typical range from 0 to 2
    s_id = "s_id_C"
    m_id = "m_id_C"
    label = "station c"
    coord = (-10.54,132.9)
    trange = (0, 2)
    river = "River c"
    town = "My Town c"
    c = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    #test station D. Has a typical range from o to 2
    s_id = "s_id_D"
    m_id = "m_id_D"
    label = "station d"
    coord = (28.537,-10.87)
    trange = (0, 2)
    river = "River d"
    town = "My Town d"
    d = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    measure_id_to_value = {
        "m_id_A": 2.0,
        "m_id_B": None,
        "m_id_C": 2.0,
        "m_id_D": 10.0,
    }
    stations = [a,b,c,d]
    for station in stations:

        if station.measure_id in measure_id_to_value:
            if isinstance(measure_id_to_value[station.measure_id], float):
                station.latest_level = measure_id_to_value[station.measure_id]
    
    #Setting the tolerance to 2.0
    test = stations_level_over_threshold(stations,2.0)  
    
    #Should only print station d, as the a and b have none types somewhere and the relative level for C is too low
    assert test == [('station d', 5.0)]
     



def test_stations_highest_rel_level():
    # test station a. Has a typical range of 2
    s_id = "s_id_A"
    m_id = "m_id_A"
    label = "station a"
    coord = (-10.54,132.9)
    trange = (0, 2)
    river = "River a"
    town = "My Town a"
    a = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    #test station B. Has a typical range of 2
    s_id = "s_id_B"
    m_id = "m_id_B"
    label = "station b"
    coord = (28.537,-10.87)
    trange = (0, 2)
    river = "River b"
    town = "My Town b"
    b = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    
    #A has the higher relative level
    measure_id_to_value = {
        "m_id_A": 2.0,
        "m_id_B": 0.0,
    }

    stations = [a,b]
    for station in stations:
        station.latest_level = None

        if station.measure_id in measure_id_to_value:
            if isinstance(measure_id_to_value[station.measure_id], float):
                station.latest_level = measure_id_to_value[station.measure_id]

    test = stations_highest_rel_level(stations,1)
#Should print out A and its relative level only as only the first station has been called
    assert test == [('station a', 1.0)]