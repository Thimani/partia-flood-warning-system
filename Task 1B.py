from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
def run ():
    "The requirements for Task 1B"
    station_name_list = build_station_list ()
    sorted_station_list = stations_by_distance(station_name_list,(52.2053,0.1218))
   
    

    ten_closest =  sorted_station_list[:10]
    ten_furthest = sorted_station_list[-10:]

    print ("The ten closest stations are:", ten_closest)
    print ("The ten closest stations are:", ten_furthest)

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()