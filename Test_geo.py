from floodsystem import geo
from floodsystem import station

#testing stations_by_distance
def geo_test():

    # test station a. Has the same river as test station d
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station a"
    coord = (-10.54,132.9)
    trange = (-2.3, 3.4445)
    river = "River a"
    town = "My Town a"
    a = station.MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    #test station B. Has a station but is out of the radial limit
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station b"
    coord = (28.537,-10.87)
    trange = (-2.3, 3.4445)
    river = "River b"
    town = "My Town b"
    b = station.MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    #test station c. Does not have a river
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station c"
    coord = (163,29.78)
    trange = (-2.3, 3.4445)
    river = None
    town = "My Town c"
    c = station.MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    #test station d. has the same river as station a
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station d"
    coord = (-19.28,187.2)
    trange = (-2.3, 3.4445)
    river = "River a"
    town = "My Town d"
    d = station.MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    #A list consisting of the four stations created above to test the functions
    test_stations = (a,b,c,d)

    # the first function, stations_by_distance is being tested here, using a centre 10,500
    distance_test = geo.stations_by_distance (test_stations,(10,500))

    #Testing the output as should be expected from the function. The haversine values have been calculated in a seperate file, so the values to expect are known
    assert distance_test[0] == ("station a", "My Town a", 2415.1594084626377)
    assert distance_test[1] == ("station d", "My Town d", 6110.439740968255)
    assert distance_test[2] == ("station c", "My Town c", 7549.817769259384)
    assert distance_test[3] == ("station b", "My Town b",  14709.089952174712)

    #Test for the stations_within_radius function
    radius_test = geo.stations_within_radius(test_stations,(10,500),10000)

    #From calculations, station b should not be within the list 
    assert radius_test[0] == "station a"
    assert radius_test[1] == "station c"
    assert radius_test[2] == "station d"
    #only the remaining three stations should be included
    assert len(radius_test) == 3

    
    #rivers_with_station function test
    river_stations_test = geo.rivers_with_station(test_stations)

    #Stations A and D share River a and there is one other river only, river b
    assert river_stations_test == {"River a", "River b"}

    #Testing the stations_by_river function
    stations_river_test = geo.stations_by_river(test_stations)

    #For river a ther are both stations a and d, river b has station b and as station c has no associated river it should not appear in the dictionary
    assert stations_river_test == {"River a": ["station a", "station d"], "River b": ["station b"] }


geo_test()

def test_rivers_by_station_number():
    """the function for 1E, first N terms"""
    station_list = stations_test()
    river_list = rivers_by_station_number(station_list, 5)

    #for two items in the list, check their types
    for item in river_list:
        assert len(item) == 2
        assert type(item[0]) == str
        assert type(item[1]) == int

    #when N=4, check length of list is at least 4
    assert len(river_list) >= 4

    #test for expected value from demonstration result
    for item in river_list:
        assert item[0] in ('River Parrett', 'River South Tyne', 'River Cam', 'River Swale', 'River Dove', 'Nettleham Beck', 'River Glen')
    assert len(rivers_by_station_number(station_list, 1)) == 1
    assert len(rivers_by_station_number(station_list, 2)) == 2
    assert len(rivers_by_station_number(station_list, 3)) == 7
    assert rivers_by_station_number(station_list, 1)[0] == ('River Cam', 3)
    assert rivers_by_station_number(station_list, 2)[1] == ('River Dove', 2)