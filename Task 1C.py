from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    "The requirements for task 1C"
    station_name_list = build_station_list()
    radial_station_list = stations_within_radius (station_name_list,(52.2053,0.1218),10)
    radial_station_list = sorted(radial_station_list)

    print (radial_station_list)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()