from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    stations = build_station_list ()
    update_water_levels(stations)
    for stations_levels in stations_level_over_threshold(stations, 0.8):
        print (stations_levels)
    

if __name__ == "__main__":
    print("*** Task 2B: ';l,m  CUED Part IA Flood Warning System***")
    run()

