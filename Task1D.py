from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list 
def run():
    #assigning the functions to variables for ease of calling in later coding
    stations = build_station_list()
    rivers = rivers_with_station(stations)
    station_rivers = stations_by_river(stations)
    #Calculating the length of the list and printing the list length
    print ("The number of rivers that have at least one Monitoring Station is: ", len(rivers))
    
    #sorting the list alphabetically and outputting the first ten entries
    sorted_river_station_list = sorted(rivers)
    print (sorted_river_station_list[:10])
    
    #"River Aire" is the key in the dictionary created amd below the data assigned to the key is being recalled, sorted alphabetically and printed
    river_aire = station_rivers["River Aire"]
    print ("RIVER AIRE: ", sorted(river_aire))

    river_cam = station_rivers["River Cam"]
    print ("RIVER CAM: ", sorted(river_cam))

    river_thames = station_rivers["River Thames"]
    print ("RIVER THAMES: ", sorted(river_thames))




if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()