# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list




def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town



def test_inconsistent_typical_range_stations():
    """test list in 1F"""
    #test for its type 
    stations= build_station_list()
    test_inconsistent_list=inconsistent_typical_range_stations(stations)
    
    #test for the expected values
    assert test_inconsistent_list==['Braunton', 'Littlehampton', 'Hull Barrier Victoria Pier', 'Airmyn', 'Truro Harbour', 'Salt end', 'Fleetwood', 'Brentford', 'Hedon Thorn Road Bridge', 'Broomfleet Weighton Lock', "King's Lynn", 'Hedon Westlands Drain', 'Blacktoft', 'Goole', 'Totnes', 'Sindlesham Mill', 'Liverpool', 'Hull High Flaggs, Lincoln Street', 'East Hull Hedon Road', 'Templers Road', 'Wilfholme PS Hull Level', 'Paull', 'Stone Creek', 'Wilfholme PS', 'Topsham', 'Medmerry']

test_create_monitoring_station()
test_inconsistent_typical_range_stations()