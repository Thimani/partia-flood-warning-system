from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels, plot_water_level_with_fits
from floodsystem.datafetcher import fetch_measure_levels
import datetime

def run():
    """Requirements for Task 2E"""

    #Build list
    stations = build_station_list()

    #update water levels
    update_water_levels(stations)


    #get highest level 5 stations
    high_risk_list = stations_highest_rel_level(stations, 5)
    
    #find the data of 2days and plot the graph

    dt = 2
    for station in high_risk_list:
        for target_station in stations:
            if station.name == target_station.name:
                dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
                plot_water_level_with_fits(station,dates,levels,4)
                break
