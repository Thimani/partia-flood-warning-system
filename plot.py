import numpy as np 
import matplotlib.pyplot as plt
from .utils import sorted_by_key
from floodsystem.datafetcher import fetch_measure_levels
from datetime import datetime, timedelta


def plot_water_levels(station, dates, levels):
    t = dates
    level = levels

    plt.plot(t, level)

    #add axis labels
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    Lower, Upper = station.typical_range
    plt.plot (dates, np.full(len(dates),Lower))
    plt.plot (dates, np.full(len(dates),Upper))



    #displaying the plot
    plt.tight_layout()  
    plt.show ()
    
    