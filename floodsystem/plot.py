import numpy as np 
import matplotlib.pyplot as plt
from floodsystem.station import MonitoringStation

def plot_water_levels(station, dates, levels):

    station_name = station[0].name
    min_level = station[0].typical_range[0]
    max_level = station[0].typical_range[1]
    length = len(dates)

    dates = np.array(dates).reshape((1, length))
    levels = np.array(dates).reshape((1,length))

    plt.plot(dates [0], levels[0], label = "Water levels over the past 10 days")
    plt.axhline(y=min_level, colour = "blue")
    plt.axhline(y=max_level, colour = "blue")
    #add axis labels
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station_name)

    #displaying the plot
    plt.tight_layout()  
    plt.show ()
    
    return None