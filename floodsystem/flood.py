from .utils import sorted_by_key
from floodsystem.stationdata import update_water_levels, build_station_list
#Creating the function for Task 2B
def stations_level_over_threshold(stations, tol):
    level_over_tol = []
    for station in stations:
        relative_level = station.relative_water_level()
        #Consistency test
        if relative_level != None and relative_level > tol:
            data = (station.name, relative_level)
            level_over_tol.append(data)                
    #Sorting the list to be in reverse order based off of relative levels
    level_over_tol = sorted_by_key(level_over_tol,1, reverse = True)
    return level_over_tol

#Creating the function for Task 2C
def stations_highest_rel_level(stations,N):
    highest_level_water = []
    for i in stations:
        related = i.relative_water_level()
        #accounts for non-types
        if related != None:
            highest_level_water.append((i.name,related))
    highest_level_water = sorted_by_key (highest_level_water,1, reverse = True)
    return highest_level_water [:N]