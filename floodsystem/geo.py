# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.
Haversine module from https://pypi.org/project/haversine/
"""
import pytest
#importing the haversine function to calculate distances
from haversine import haversine, Unit
from collections import defaultdict
from .utils import sorted_by_key  # noqa


def stations_by_distance (stations,p):
    #creating an empty list
    station_list = []
    #adding values to the list 
    for station in stations:
        station_list.append((station.name, station.town, haversine(station.coord, p, unit = Unit.KILOMETERS)))
    #sorting the list based off the 3rd piece of each entry, the distance
    return sorted_by_key(station_list,2)


def stations_within_radius(stations,centre,r):
    radial_locations = []
    for i in stations:
        #setting the conditions for the radius within which the station must be
        if haversine (i.coord,centre)<int (r):
            radial_locations.append(i.name)
        #sorting the list values in alphabetical order
        radial_locations = sorted_by_key(radial_locations,0)
    else:
        pass
    return radial_locations

def rivers_with_station(stations):
    rivers_with_stations = set()
    for i in stations: 
        #given there is a river for the given station
        if i.river != None:
            rivers_with_stations.add(i.river)
        else:
            pass
    return rivers_with_stations

def stations_by_river(stations):
    river_station = defaultdict(list)
    for i in stations:
        if i.river != None:
            #the key is the river name and it holds the station names
                river_station[i.river].append(i.name) 
        else:
            pass
    return river_station





def rivers_by_station_number(stations, N):
    """ determines the N rivers with the greatest number of monitoring stations,
    returns a list of (river name, number of stations) tuples, sorted by the number of stations,
    in the case that there are more rivers with the same number of stations as the N th entry,
    include these rivers in the list """

    #Get the dictionary of rivers to stations from Task 1D
    river_dictionary = stations_by_river(stations)

    #Make a list of tuples containing (river, number of stations)
    river_list = []
    for river in river_dictionary:
        number = len(river_dictionary[river])
        river_list.append((river, number))
    
    #Sort the list using the provided code in utils.py
    sorted_river_list = sorted_by_key(river_list, 1, reverse=True)

    #Cut off first N terms(array slicing)
    first_N_rivers = sorted_river_list[:N] #array slicing from start to N(not including N)
    remaining_rivers = sorted_river_list[N:] #array slicing from N to the end
    
    #Check if any have same as the Nth entry and add to list
    for item in remaining_rivers:
        if item[1] == first_N_rivers[N-1][1]: #if the last entry of river numbers (which is N-1) in first n rivers matches the remaining rivers 
            first_N_rivers.append(item)

    return first_N_rivers




