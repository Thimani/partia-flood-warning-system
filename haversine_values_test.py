from haversine import haversine, Unit 
#testing the haversine value for each test station

centre = (10,500)
a = (-10.54,132.9)
b = (28.537,-10.87)
c = (163,29.78)
d = (-19.28,187.2)

a_dist = haversine (centre,a, unit=Unit.KILOMETERS)
print (a_dist)
b_dist = haversine (centre,b, unit=Unit.KILOMETERS)
print (b_dist)
c_dist = haversine (centre,c, unit=Unit.KILOMETERS)
print (c_dist)
d_dist = haversine (centre,d, unit=Unit.KILOMETERS)
print (d_dist)